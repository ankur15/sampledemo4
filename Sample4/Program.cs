﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4
{
    class Program
    {
        static void Main(string[] args)
        {
			//IEnemy galax = new Galax();

			//GalaxPrime galaxPrimeAdaptee = new GalaxPrime();

			//IEnemy galaxPrime = new EnemyAdapter(galaxPrimeAdaptee);

			//// Test a regular Enemy

			//galax.moveShip();
			//galax.makeShipAttack();

			//Console.WriteLine();

			//// Test an adapted Enemy

			//galaxPrime.moveShip();
			//galaxPrime.makeShipAttack();



			ATMAccess user = new ATMAccess();

			Console.WriteLine(user.getState());

			user.verifyCard(123456789);

			Console.WriteLine(user.getState());

			user.verifyPIN(1234);

			Console.WriteLine(user.getState());

			user.verifyWithdrawalAmount(1000);

			Console.WriteLine(user.getState());

			Console.ReadKey();
		}
    }
}
