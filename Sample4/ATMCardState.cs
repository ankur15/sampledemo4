﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4
{
	class ATMCardState
	{

		private readonly String name ="";
	
	private ATMCardState(String name)
		{

			this.name = name;

		}

		public String toString()
		{

			return name;

		}

		// These type safe constants live in the class and can't be impersonated

		public static readonly ATMCardState CARD_ENTERED = new ATMCardState("CARD ENTERED");
		public static readonly ATMCardState VALID_CARD = new ATMCardState("VALID CARD");
		public static readonly ATMCardState VALID_PIN = new ATMCardState("VALID PIN");
		public static readonly ATMCardState VALID_CASH_REQUEST = new ATMCardState("VALID CASH REQUEST");
		public static readonly ATMCardState DENIED = new ATMCardState("DENIED");

		public static readonly int CARD_NUMBER = 123456789;
		public static readonly int PIN_NUMBER = 1234;
		public static readonly double CARD_BALANCE = 1000.00;

	}
}
