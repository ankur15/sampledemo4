﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4
{
	class EnemyAdapter : IEnemy
	{

		GalaxPrime galaxPrime;


	public EnemyAdapter(GalaxPrime galaxPrime)
	{	
		this.galaxPrime = galaxPrime;
	}

	public void moveShip()
	{
		galaxPrime.turnOnForceField();
		galaxPrime.warpToSpace();
	}

	public void makeShipAttack()
	{
		galaxPrime.chargePhasers();
		galaxPrime.firePhasers();
	}

}
}
