﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4
{
	class Galax : IEnemy
	{

	private int attackPower = 5;
	private int spacesMovedPerTurn = 2;

	public void moveShip()
	{

			Console.WriteLine("Galax moves " + spacesMovedPerTurn + " spaces");

	}

	public void makeShipAttack()
	{
			Console.WriteLine("Galax does " + attackPower + " damage");
	}

}
}
