﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4
{
	class GalaxPrime
	{

		protected String name = "Galaxian Prime";
		private int attackPower = 15;
		protected int spacesMovedPerTurn = 4;

		public void turnOnForceField()
		{
			Console.WriteLine(name + " turns on force field");
		}

		public void warpToSpace()
		{
			Console.WriteLine(name + " warps " + spacesMovedPerTurn + " spaces");
		}

		public void chargePhasers()
		{
			Console.WriteLine(name + " charges phasers");
		}

		public void firePhasers()
		{
			Console.WriteLine(name + " fires phasers for " + attackPower + " damage");
		}

	}
}
